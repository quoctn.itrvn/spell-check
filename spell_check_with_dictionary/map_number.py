import copy
import csv
import json

import pandas as pd
import numpy as np


def remove_special(comment):
    list_special_c = ['-', '/', ',', '`', '=', '(', ')', "'", '–', '\n', '#', '\t', '"', '&', '\xa0',
                      ']', '[', '}', '{', '▲', '\ufeff', '\u200c', '“']
    corpus = copy.deepcopy(comment)
    corpus = corpus.replace(';', ':')
    corpus = corpus.split(':')[-1]
    # corpus_splitting = pd.Series.tolist(corpus_splitting)
    for i in range(0, len(list_special_c)):
        corpus = corpus.replace(list_special_c[i], ' ')
    corpus = corpus.replace('fibrillation flutter', 'fibrillation/flutter')
    return corpus


def add_number(raw_comment, list_num_raw, processed_comment, spelling_labels=None):
    # if ':' in raw_comment or ';' in raw_comment:
    #     return raw_comment
    # else:
    # raw_corpus = remove_special(raw_comment)
    # list_num_raw = []
    # num_num = -1
    # try:
    #     for j in range(0, len(raw_corpus)):
    #         if j == 0 and raw_corpus[j].isnumeric() and raw_corpus[j + 1] not in ['s', 'n', 'r']:
    #             list_num_raw.append(raw_corpus[j])
    #             num_num += 1
    #         elif raw_corpus[j].isnumeric() and not raw_corpus[j - 1].isnumeric() \
    #                 and (j == len(raw_corpus) - 1 or raw_corpus[j + 1] not in ['s', 'n', 'r']):
    #             list_num_raw.append(raw_corpus[j])
    #             num_num += 1
    #         elif raw_corpus[j].isnumeric() and (raw_corpus[j - 1].isnumeric() or raw_corpus[j - 1] == '.'):
    #             list_num_raw[num_num] = list_num_raw[num_num] + raw_corpus[j]
    #         elif raw_corpus[j] == '.' and raw_corpus[j - 1].isnumeric():
    #             list_num_raw[num_num] = list_num_raw[num_num] + raw_corpus[j]
    # except:
    #     print(f'Origin: {raw_comment}')
    #     print(f'Processed: {processed_comment}')
    #     raise ValueError(f'Error at {raw_corpus[j]}')

    processed_corpus = remove_special(processed_comment)
    processed_corpus_splitting = processed_corpus.split(' ')
    unk_processed_corpus = np.where(np.array(processed_corpus_splitting) == '<unk>')[0]
    if len(unk_processed_corpus) != len(list_num_raw):
        print(f'Origin: {raw_comment}')
        print(f'Check spelling: {spelling_labels}')
        print(f'Processed: {processed_comment}')
        print('\n')
        return None
    else:
        for k in range(0, len(unk_processed_corpus)):
            processed_corpus_splitting[unk_processed_corpus[k]] = list_num_raw[k]
        final_result = ' '.join(processed_corpus_splitting)
        if '1 st' in final_result:
            final_result = final_result.replace('1 st', '1st')
        if '2 nd' in final_result:
            final_result = final_result.replace('2 nd', '2nd')
        if '3 rd' in final_result:
            final_result = final_result.replace('3 rd', '3rd')
        return final_result


if __name__ == '__main__':
    name_processed_file = 'all_events_with_comments_spelling.csv'

    name_final_file = 'all_events_with_comments_final.csv'
    name_json_file = 'all_events_with_comments_spelling.json'

    name_missing_file = 'all_events_with_comments_spelling_predict_missing_1.csv'
    _df = pd.read_csv(name_processed_file)
    _df_2 = pd.read_csv(name_missing_file)
    labels = _df['Raw Labels']
    spelling_labels = _df['Processed Labels']
    # labels[0] = 'sinus bradycardia at 47 bpm with 1st degree av block and second degree av block, type 1'
    processed_labels = _df_2['Processed Labels']
    id = _df['PseudoID']
    wrong = 0
    with open(name_json_file, 'r') as f:
        dict_num_unk = json.load(f)
    with open(name_final_file, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['PseudoID', 'Raw Labels', 'Processed Labels', 'Final Labels'])
    for i in range(0, len(labels)):
        final_label = add_number(labels[i], dict_num_unk[id[i]], processed_labels[i], spelling_labels[i])
        if final_label is not None:
            with open(name_final_file, 'a') as f:
                writer = csv.writer(f)
                writer.writerow([id[i], labels[i], processed_labels[i], final_label])
        else:
            wrong += 1
    print(f'Num wrong', wrong)
