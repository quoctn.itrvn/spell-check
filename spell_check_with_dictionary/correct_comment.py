import copy
import csv
import json
import time

import numpy as np

# from vocab import Vocabulary
# from collections import Counter
import pandas as pd
from cdifflib import CSequenceMatcher
import re

dictionary = ['st', 'nd', 'rd',
              'aberration', 'accelerated', 'advanced', 'and', 'arrhythmia', 'artifact', 'at', 'atrial', 'auricular',
              'av', 'any',
              'baseline', 'beats', 'bigeminy', 'biphasic', 'block', 'bpm', 'bradycardia', 'breath', 'business', 'by',
              'call', 'cardiology', 'care', 'cct', 'cdt', 'cell', 'changes', 'clinic', 'comments', 'comstock',
              'conduction', 'contact', 'converting', 'correction', 'connected', 'couplets', 'course', 'criteria', 'cst',
              'date', 'dizziness', 'define', 'degree', 'detected', 'difficult', 'directly', 'do', 'dropped', 'dual',
              'due', 'duration',
              'ectopic', 'email', 'emergent', 'enough', 'episodes', 'escape', 'event', 'expire', 'excision',
              'fc', 'fibrillation', 'fibrillation/flutter', 'followed', 'for', 'from', 'flutter',
              'grade', 'gradual', 'had', 'hard', 'have', 'heart', 'high', 'hours',
              'id', 'idioventricular', 'in', 'including', 'intermittent', 'interpolated', 'interval', 'into', 'invalid',
              'inverted', 'ivcd',
              'junctional', 'just',
              'lasted', 'leads', 'left', 'less', 'longest', 'loss',
              'max', 'message', 'minutes', 'more', 'morphology', 'multifocal',
              'need', 'new', 'no', 'nonconducted', 'noted', 'notification', 'notify', 'number', 'nurse',
              'of', 'off', 'office', 'offset', 'on', 'onset', 'operator', 'other',
              'pac', 'pacs', 'paced', 'pacemaker', 'paroxysmal', 'pause', 'pjc', 'pjcs', 'polymorphic', 'possible', 'p',
              'pre', 'pvt',
              'patient', 'phone', 'physician', 'polymorphic', 'possible', 'posted', 'practitioner', 'progressing',
              'prolonged', 'psvt', 'pvc', 'pvcs',
              'qrs', 'qt', 'quadrigeminy', 'questions',
              'ranging', 'rate', 'reason', 'reported', 'reversed', 'rhythm', 'run', 'rb',
              'second', 'seconds', 'see', 'send', 'shortest', 'shows', 'single', 'sinus', 'spoke', 'standstill',
              'stated',
              'study', 'support', 'supraventricular', 'sve', 'sves', 'svt', 'symptomatic',
              'tachycardia', 'technician', 'terminating', 'text', 'than', 'the', 'then', 'time', 'to', 'took',
              'trigeminy', 'type',
              'up', 'urgent',
              'variable', 've', 'ventricular', 'ves', 'vs', 'view', 'voicemail',
              'was', 'wave', 'will', 'with',
              '<unk>']


def match_and_split(corpus_splitting_k, index_word_chosen, dictionary, new_labels, list_unk_num):
    origin_labels = copy.deepcopy(new_labels)
    if dictionary[index_word_chosen] in corpus_splitting_k \
            and len(corpus_splitting_k) > len(dictionary[index_word_chosen]):
        remain_word = corpus_splitting_k.replace(dictionary[index_word_chosen], '')
        remain_word_pro = map_to_dictionary(corpus_splitting_k.replace(dictionary[index_word_chosen], ''),
                                            dictionary)
        if dictionary[np.argmax(remain_word_pro)] in remain_word:
            if dictionary[index_word_chosen] == corpus_splitting_k[:len(dictionary[index_word_chosen])]:
                new_labels = new_labels + dictionary[index_word_chosen] + ' '
                if len(remain_word) >= len(dictionary[np.argmax(remain_word_pro)]):
                    new_labels, list_unk_num = match_and_split(remain_word, np.argmax(remain_word_pro), dictionary, new_labels, list_unk_num)
            else:
                if len(remain_word) >= len(dictionary[np.argmax(remain_word_pro)]):
                    new_labels, list_unk_num = match_and_split(remain_word, np.argmax(remain_word_pro), dictionary, new_labels, list_unk_num)
                new_labels = new_labels + dictionary[index_word_chosen] + ' '

        else:
            new_labels = new_labels + dictionary[index_word_chosen] + ' '
    else:
        new_labels = new_labels + dictionary[index_word_chosen] + ' '
    if '<unk>' in corpus_splitting_k and '<unk>' not in new_labels[len(origin_labels):]:
        new_labels_splitting = new_labels.split(' ')
        unk_index = np.where(np.array(new_labels_splitting) == '<unk>')[0]
        if len(unk_index):
            del list_unk_num[unk_index[-1] + 1]

        else:
            del list_unk_num[0]

    return new_labels, list_unk_num


def map_to_dictionary(analyzed_word, dictionary):
    word_pro = np.zeros(len(dictionary))
    for m in range(0, len(dictionary)):
        s = CSequenceMatcher(None, analyzed_word, dictionary[m]).ratio()
        word_pro[m] = copy.deepcopy(s)
    return word_pro


def remove_duplicate(corpus_splitting, list_unk_num):
    urgent_index = np.where((np.array(corpus_splitting) == 'urgent') | (np.array(corpus_splitting) == 'emergent'))[0]
    if len(urgent_index):
        corpus_splitting = corpus_splitting[urgent_index[0]:]
        count_unk = len(np.where(np.array(corpus_splitting) == '<unk>')[0])
        list_unk_num = list_unk_num[len(list_unk_num) - count_unk:]
    i = 1
    while i < len(corpus_splitting):
        if corpus_splitting[i] == corpus_splitting[i - 1] and corpus_splitting[i] != '<unk>':
            corpus_splitting.remove(corpus_splitting[i])
        else:
            i += 1
    return corpus_splitting, list_unk_num


def process_comments(label_, dictionary):
    label = label_.replace('type ii', 'type 2')
    label = label.replace('type i', 'type 1')
    label = label.replace('avb', 'av block')
    label = label.replace('first', '1st')
    label = label.replace('afib', 'atrial fibrillation')

    corpus = re.sub(r'\d*\.?\d+', '<unk>', label)
    list_unk_num = re.findall('\d*\.?\d+', label)

    new_labels = ''
    list_special_c = ['-', '/', ',', '`', '=', '(', ')', "'", '–', '\n', '#', '\t', '"', '&', '\xa0',
                      ']', '[', '}', '{', '▲', '\ufeff', '\u200c', '“', ':', ';']

    # corpus_splitting = pd.Series.tolist(corpus_splitting)
    for i in range(0, len(list_special_c)):
        corpus = corpus.replace(list_special_c[i], ' ')
    corpus = corpus.replace('fibrillation flutter', 'fibrillation/flutter')
    corpus_splitting = corpus.split(' ')

    while '' in corpus_splitting:
        corpus_splitting.remove('')
    while '\\' in corpus_splitting:
        corpus_splitting.remove('\\')
    k = 0
    # list_num_unk = []
    while k < len(corpus_splitting):
        max_chose = 0
        analyzed_word = ''
        num_loop = -1
        remember_loop = 0
        while True:
            num_loop += 1
            try:
                analyzed_word = analyzed_word + corpus_splitting[k + num_loop]
            except:
                break
            word_pro = map_to_dictionary(analyzed_word, dictionary)
            if max_chose <= max(word_pro) and (
                    dictionary[np.argmax(word_pro)][0] == corpus_splitting[k][0] or num_loop == 0):
                max_chose = max(word_pro)
                index_word_chosen = int(np.argmax(word_pro))
                remember_loop = copy.deepcopy(num_loop)
            else:
                break
        new_labels, list_unk_num = match_and_split(corpus_splitting[k], index_word_chosen, dictionary, new_labels, list_unk_num)
        k += remember_loop + 1
    new_corpus_splitting = new_labels.split(' ')
    new_corpus_splitting, list_unk_num = remove_duplicate(new_corpus_splitting, list_unk_num)
    new_labels = ' '.join(new_corpus_splitting)
    return new_labels, list_unk_num


if __name__ == '__main__':
    t0 = time.process_time()
    name_raw_file = 'try_raw_comments.csv'
    name_processed_file = 'try_raw_comments_spelling.csv'
    name_processed_file_json = 'try_raw_comments_spelling.json'

    _df = pd.read_csv(name_raw_file)
    labels = _df['Label']
    id = _df['PseudoID']
    new_labels = []
    dict_num_unk = dict()
    with open(name_processed_file, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['PseudoID', 'Raw Labels', 'Processed Labels'])
    for j in range(0, len(labels)):
        try:
            new_label, list_num_unk = process_comments(labels[j], dictionary)
            new_labels.append(new_label)
            dict_num_unk[id[j]] = list_num_unk

            with open(name_processed_file, 'a') as f:
                writer = csv.writer(f)
                writer.writerow([id[j], labels[j], new_label])
        except:
            continue
    with open(name_processed_file_json, 'w') as f1:
        json.dump(dict_num_unk, f1)
    print(f'Time is: {time.process_time() - t0}')
