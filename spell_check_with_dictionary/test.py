import numpy as np
from spectral import *
import matplotlib.pyplot as plt

# x = open_image('pavia.npy')
x = np.load('pavia.npy')
print(x.shape)

for i in range(90, 92):
    plt.imshow(x[50:200, 50:200, i], cmap='jet')
    plt.show()

