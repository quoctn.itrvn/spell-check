import copy
import csv
import os
import time

import numpy as np
import pandas as pd
import re

import wfdb
from map_number import add_number
from correct_comment import process_comments, dictionary
from dateutil.parser import parse


def change_special_num(spelling_comment, list_unk_num):
    corpus_splitting = spelling_comment.split(' ')
    list_unk = np.where(np.array(corpus_splitting) == '<unk>')[0]
    list_unk_before = list_unk - 1
    list_unk_before = np.where(list_unk_before < 0, 0, list_unk_before)
    type_index = np.where(np.array(np.array(corpus_splitting)[list_unk_before]) == 'type')[0]
    training_comment = copy.deepcopy(spelling_comment)
    if '<unk> st' in training_comment:
        training_comment = training_comment.replace('<unk> st', '1st')
    if '<unk> nd' in training_comment:
        training_comment = training_comment.replace('<unk> nd', '2nd')
    if '<unk> rd' in training_comment:
        training_comment = training_comment.replace('<unk> rd', '3rd')
    if 'type <unk>' in training_comment:
        training_comment = training_comment.replace('type <unk>', f'type {list_unk_num[type_index[0]]}')

    return training_comment


if __name__ == '__main__':
    base_data_path = "/home/ai_dev_01/crawl-event/ECG/PortalData/RhythmNet/2022-05-24"  # year - month - day
    name_raw_file = 'all_events_with_comments.csv'
    name_processed_file = 'all_events_with_comments_20220524_020622.csv'
    _df = pd.read_csv(name_raw_file)
    labels = _df['Labels']
    id = _df['PseudoID']
    with open(name_processed_file, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(['PseudoID', 'Raw Labels', 'Spell-Checked Labels', 'Training Labels', 'Number-Mapped Labels'])

    # labels = ['afib at 89 bpm']
    for i in range(0, len(labels)):
        try:
            raw_signal, fields = wfdb.rdsamp(os.path.join(base_data_path, id[i]))
            if len(raw_signal) != 15000:
                continue
            if len(labels[i].split()) == 1 and labels[i].split()[0] not in ['artifact', 'afib']:
                continue

            spelling_comment, list_unk_num = process_comments(labels[i], dictionary)

            if len(spelling_comment.split()) == 1 and spelling_comment.split()[0] not in ['artifact', 'afib']:
                continue
            training_comment = change_special_num(spelling_comment, list_unk_num)

            final_result = add_number(labels[i], list_unk_num, spelling_comment)

            with open(name_processed_file, 'a') as f:
                writer = csv.writer(f)
                writer.writerow([id[i], labels[i], spelling_comment, training_comment, final_result])
        except:
            pass
#
