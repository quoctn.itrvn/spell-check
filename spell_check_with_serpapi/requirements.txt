google_search_results==2.4.1
pandas==1.4.2
pydub==0.25.1
requests==2.27.1
selenium==3.141.0
SpeechRecognition==3.8.1
webdriver_manager==3.5.4
