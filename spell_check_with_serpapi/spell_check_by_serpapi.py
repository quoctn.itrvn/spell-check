from serpapi import GoogleSearch
import pandas as pd

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager
import captcha_bypass
import time


def get_serpapi_api_key(email, password):
    api_key = ''
    try:
        driver = webdriver.Chrome(ChromeDriverManager().install())
        # driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())

        driver.get('https://serpapi.com/users/sign_in')
        driver.implicitly_wait(15)

        login_box = driver.find_element_by_xpath('//*[@id="user_email"]')
        login_box.send_keys(email)

        password_box = driver.find_element_by_xpath('//*[@id="user_password"]')
        password_box.send_keys(password)

        signin_button = driver.find_elements_by_xpath('//*[@id="new_user"]/div[5]/input')
        signin_button[0].click()
        driver.switch_to.window(driver.window_handles[-1])

        if 'https://serpapi.com/users/sign_in' in driver.current_url:
            raise Exception('Invalid email or password')

        if 'https://serpapi.com' in driver.current_url:
            driver.get('https://serpapi.com/users/welcome')
            driver.switch_to.window(driver.window_handles[-1])

        if 'https://serpapi.com/users/welcome' in driver.current_url:
            subscribe_button = driver.find_element_by_xpath('//*[@id="btnSubmit"]')
            subscribe_button.click()
            # Filter through all the iframes on the page and find the one that corresponds to the captcha
            iframes = driver.find_elements_by_tag_name("iframe")
            for iframe in iframes:
                if iframe.get_attribute("src").startswith("https://www.google.com/recaptcha/api2/anchor"):
                    captcha = iframe

            result = captcha_bypass.solve_captcha(driver, captcha)

        try:
            driver.get('https://serpapi.com/manage-api-key')
            api_key = driver.find_element_by_id('private-api-key').get_attribute('value')
        except:
            print('Get API Key Failed')
            driver.close()
            return api_key

        driver.close()

        print('Get API Key successfully...!!')

        return api_key

    except Exception as e:
        print('Get API Key failed...!!', e)
        return api_key


def signup_serpapi(email, password):
    try:
        driver = webdriver.Chrome(ChromeDriverManager().install())
        # driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())

        driver.get('https://serpapi.com/users/sign_up')
        # driver.implicitly_wait(15)

        fullname_box = driver.find_element_by_xpath('//*[@id="full_name"]')
        fullname_box.send_keys('quoc')

        email_box = driver.find_element_by_xpath('//*[@id="email"]')
        email_box.send_keys(email)

        password_box = driver.find_element_by_xpath('//*[@id="user_password"]')
        password_box.send_keys(password)

        confirm_password_box = driver.find_element_by_xpath('//*[@id="user_password_confirmation"]')
        confirm_password_box.send_keys(password)

        signup_button = driver.find_elements_by_xpath('//*[@id="sign-up-button"]')
        signup_button[0].click()

        # Filter through all the iframes on the page and find the one that corresponds to the captcha
        iframes = driver.find_elements_by_tag_name("iframe")
        for iframe in iframes:
            if iframe.get_attribute("src").startswith("https://www.google.com/recaptcha/api2/anchor"):
                captcha = iframe

        result = captcha_bypass.solve_captcha(driver, captcha)

        driver.implicitly_wait(15)

        # driver.get('https://serpapi.com/users/welcome')
        # subscribe_button = driver.find_element_by_xpath('//*[@id="btnSubmit"]')
        # subscribe_button[0].click()

        driver.close()

        print('Sign up successfully...!!')

    except Exception as e:
        print('Sign up Failed', e)


def access_email(email, password):
    try:
        driver = webdriver.Chrome(ChromeDriverManager().install())
        # driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        driver.delete_all_cookies()

        driver.get('https://mail.google.com')
        driver.implicitly_wait(15)

        email_box = driver.find_element_by_xpath('//*[@id="identifierId"]')
        email_box.send_keys(email)

        next_button = driver.find_elements_by_xpath('//*[@id="identifierNext"]/div/button')
        next_button[0].click()

        password_box = driver.find_element_by_xpath('//*[@id="password"]/div[1]/div/div[1]/input')
        password_box.send_keys(password)

        next_button = driver.find_elements_by_xpath('//*[@id="passwordNext"]/div/button')
        next_button[0].click()

        latest_mail = driver.find_element_by_xpath('//*[@id=":2g"]')
        latest_mail.click()

        list_contents = driver.find_elements_by_class_name('gs')
        specific_content = list_contents[-1]
        if len(list_contents) >= 2:
            hide_show_button = specific_content.find_element_by_class_name('ajR')
            hide_show_button.click()
        # links = specific_content.find_elements_by_css_selector(r'a')
        links = specific_content.find_element_by_xpath('//a[contains(text(), "Confirm your email")]')
        links.click()

        driver.implicitly_wait(15)

        # subscribe_button = driver.find_element_by_xpath('//*[@id="btnSubmit"]')
        # subscribe_button.click()

        driver.close()

        print('Sign up successfully...!!')

    except Exception as e:
        print('Sign up Failed', e)


def get_valid_api_key(idx, valid_alias_emails):
    api_key = ''
    while not len(api_key):
        if idx == len(valid_alias_emails):
            print("all email addresses are exhausted!")
            break

        api_key = get_serpapi_api_key(valid_alias_emails[idx]['email'], valid_alias_emails[idx]['password'])
        idx += 1

    return api_key, idx


if __name__ == '__main__':
    # # Create the primary google account manually
    # primary_email = 'quoc.tn.1998.itrvn@gmail.com'
    # primary_password = 'quocquy12'
    #
    # # Define email aliases for primary google account
    # valid_alias_emails = [
    #     {'email': '.quoc.tn.1998.itrvn@gmail.com', 'password': 'quocquy12'},
    #     {'email': 'q.uoc.tn.1998.itrvn@gmail.com', 'password': 'quocquy12'},
    #     {'email': 'qu.oc.tn.1998.itrvn@gmail.com', 'password': 'quocquy12'},
    #     {'email': 'q.u.o.c.tn.1998.itrvn@gmail.com', 'password': 'quocquy12'},
    #     {'email': 'quoc.t.n.1998.itrvn@gmail.com', 'password': 'quocquy12'}
    # ]

    # Create the primary google account manually
    primary_email = 'quoctn1998itrvnitrvn1998quoctn@gmail.com'
    primary_password = 'quocquy12'

    # Define email aliases for primary google account
    valid_alias_emails = [
        {'email': 'quoctn1998itrvnitrvn1998quoctn@gmail.com', 'password': 'quocquy12'},
        {'email': 'q.uoctn1998itrvnitrvn1998quoctn@gmail.com', 'password': 'quocquy12'},
        {'email': 'qu.octn1998itrvnitrvn1998quoctn@gmail.com', 'password': 'quocquy12'},
        {'email': 'quo.ctn1998itrvnitrvn1998quoctn@gmail.com', 'password': 'quocquy12'},
        {'email': 'quoc.tn1998itrvnitrvn1998quoctn@gmail.com', 'password': 'quocquy12'},
        {'email': 'quoct.n1998itrvnitrvn1998quoctn@gmail.com', 'password': 'quocquy12'}
    ]

    # # Processing email
    # # Run this first for signed up - confirmed email to SerpAPI
    # for alias_email in valid_alias_emails:
    #
    #     # Sign up email aliases to Google SerpAPI
    #     signup_serpapi(alias_email['email'], alias_email['password'])
    #     time.sleep(3)
    #
    #     # Access and confirm email for SerpAPI verification
    #     access_email(primary_email, primary_password)
    #     time.sleep(3)

    # Must have signed up - confirmed email to SerpAPI
    idx = 0
    api_key, idx = get_valid_api_key(idx, valid_alias_emails)

    if len(api_key):
        corrected_labels = []

        df = pd.read_csv('event_caption_data.csv')
        for i in range(len(df['Label'])):
            # get raw labels
            raw_label = df['Label'][i]

            # set up params and call SerpAPI
            params = {
                "api_key": api_key,
                "q": raw_label,
                "gl": "us",
                "hl": "en"
            }
            search = GoogleSearch(params)
            results = search.get_dict()

            if results.get('error') and 'Your searches for the month are exhausted' in results.get('error'):
                print('Email {} is exhausted'.format(valid_alias_emails[idx]['email']))

                api_key, idx = get_valid_api_key(idx, valid_alias_emails)

                params = {
                    "api_key": api_key,
                    "q": raw_label,
                    "gl": "us",
                    "hl": "en"
                }
                search = GoogleSearch(params)
                results = search.get_dict()

            if results.get('search_information'):
                corrected_label = results.get('search_information').get('spelling_fix', raw_label)
                corrected_labels.append(corrected_label)
            else:
                print('Unknown error')
                break

        # save result
        data = pd.DataFrame({'Id': df['PseudoID'], 'Label': df['Label'], 'CorrectedLabel': corrected_labels})
        data.to_csv('corrected_all_event_caption_data')
